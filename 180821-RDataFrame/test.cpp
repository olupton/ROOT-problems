#include <ROOT/RDataFrame.hxx>

struct DecayAngles {
  Double_t phi{0.0};
};

int main()
{
  ROOT::RDataFrame d(10);
  d.Define( "foo", [](){ return DecayAngles(); } );
  return 0;
}

void test()
{
  main();
}
