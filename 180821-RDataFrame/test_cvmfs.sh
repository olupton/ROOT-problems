. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/${@}/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
MINOR=`echo ${@} | cut -d . -f 2`
if (( ${MINOR} < 14 ))
then
  HEADER="ROOT/TDataFrame.hxx"
  TYPE="ROOT::Experimental::TDataFrame"
else
  HEADER="ROOT/RDataFrame.hxx"
  TYPE="ROOT::RDataFrame"
fi

cat > test.cpp <<Endofmessage
#include <${HEADER}>

struct DecayAngles {
  Double_t phi{0.0};
};

int main()
{
  ${TYPE} d( 10 );
  d.Define( "foo", [](){ return DecayAngles(); } );
  return 0;
}

void test()
{
  main();
}
Endofmessage

echo "Building"
`root-config --cxx` -o test test.cpp `root-config --cflags --libs`
echo "Running"
./test
echo "Running as macro"
root -b -q -l test.cpp+
