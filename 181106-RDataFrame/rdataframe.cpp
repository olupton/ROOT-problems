#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <string>

int rdataframe()
{
  TChain input_chain( "MCDecayTree" );
  std::string prefix{"root://eosuser.cern.ch//eos/user/o/olupton/forROOT/181105/"};
  for ( auto i = 0; i < 2 /* warning disappears if this is changed to 1 */; ++i ) {
    input_chain.Add( ( prefix + std::to_string( i ) + ".root" ).c_str() );
  }
  std::cout << input_chain.GetEntries() << " input entries" << std::endl;
  ROOT::RDataFrame d_input( input_chain );
  std::cout << *d_input.Count() << std::endl;
  return 0;
}
