#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <string>

int rdataframe()
{
  ROOT::EnableImplicitMT();

  // Make a chain using the input files
  TChain input_chain( "MCDecayTree" );
  std::string prefix{"root://eosuser.cern.ch//eos/user/o/olupton/forROOT/181105/"};
  for ( auto i = 0; i < 14; ++i ) {
    input_chain.Add( ( prefix + std::to_string( i ) + ".root" ).c_str() );
  }

  // Load the unbinned input events into a RDataFrame
  ROOT::RDataFrame d_input( input_chain );
  auto d_weights = d_input.Define( "nw", "1.0" ).Define( "aw", "nw" );
  auto processed = d_weights.Count();

  std::string out_weight_filename{"test.root"};
  auto sshot = d_weights.Snapshot( "WeightTree", out_weight_filename, {"aw", "nw"} );

  // The snapshot should also have evaluated 'processed'
  ULong64_t n_processed = *processed;
  std::cout << "Processed " << n_processed << " entries" << std::endl;

  // Double-check that the weight tree has the correct number of entries
  std::unique_ptr<TFile> check{TFile::Open( out_weight_filename.c_str() )};
  TTree* weight_tree{nullptr};
  check->GetObject( "WeightTree", weight_tree );

  ULong64_t n_reread = weight_tree->GetEntries();
  if ( n_reread != n_processed ) {
    throw std::runtime_error( "Found " + std::to_string( n_reread ) + " weight entries, we should have just saved " +
                              std::to_string( n_processed ) );
  }

  std::cout << "All done, exiting..." << std::endl;

  return 0;
}
