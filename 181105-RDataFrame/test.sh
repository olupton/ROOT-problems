. /cvmfs/sft.cern.ch/lcg/views/dev4/latest/${CMTCONFIG}/setup.sh
set -e # abort on failure
for i in {1..10}
do
  echo "Running test #${i}"
  root -b -q -l rdataframe.cpp+
done
